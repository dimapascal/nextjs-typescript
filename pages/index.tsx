import Head from "next/head";
import NavbarComponent from "../components/Navbar";

export default function Home(): JSX.Element {
  const navbarItems = [
    { link: "/", child: "Home" },
    { link: "/about", child: "About" },
  ];
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <nav>
        <NavbarComponent navItems={navbarItems} />
      </nav>
      <main>Welcome to home page</main>

      <footer></footer>

      <style jsx>{``}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
