import CSS from "csstype";
export default interface StylesInterface {
  [key: string]: CSS.Properties;
}
