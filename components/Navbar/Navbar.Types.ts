import PropTypes from "prop-types";

export type Props = {
  navItems: Array<NavItem>;
};

export type NavItem = {
  link: string;
  child: JSX.Element | string;
};

export const propTypes = {
  navItems: PropTypes.array.isRequired,
};

export const defaultProps = {};
