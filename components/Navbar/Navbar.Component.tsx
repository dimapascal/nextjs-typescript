import { styles } from "./Navbar.Styles";
import { Props, NavItem, propTypes, defaultProps } from "./Navbar.Types";
import { Toolbar, AppBar, Button } from "@material-ui/core";
import Link from "next/link";

/**
 *
 * @param {object} props
 * @param {Array[Object]} props.navItems
 */

const NavbarComponent = (props: Props) => {
  const { navItems } = props;
  return (
    <AppBar position="static">
      <Toolbar>
        {navItems.map(({ link, child }: NavItem, index) => (
          <Link href={link} key={index}>
            <Button
              style={index != 0 ? styles.navbar__item : undefined}
              size="large"
              color="inherit"
            >
              {child}
            </Button>
          </Link>
        ))}
      </Toolbar>
    </AppBar>
  );
};

NavbarComponent.propTypes = propTypes;
NavbarComponent.defaultProps = defaultProps;

export default NavbarComponent;
