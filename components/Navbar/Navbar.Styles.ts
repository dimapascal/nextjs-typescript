import StylesInterface from "../../interfaces/StylesInterface";

export const styles: StylesInterface = {
  navbar__item: {
    marginLeft: "10px",
  },
};
